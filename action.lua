require("util")

require("chooseHeroPage")

require("chooseBattlePage")

require("battle")

require("buyEnergyPage")

require("fpBattlePage")

require("dialogPage")

require("cleanBag")

require("activityBattlePage")

local function adaptAllOtherPages(...)
	
	-- 所有通用的提示窗口,如完成成就,背包已满等
	adaptAllDialogPage()
	
end


function onlyBattle(...)
	mlog("only battle")
	while(true) do
		adaptAllOtherPages()
		
		if isBattleBegin() then
			beginBattle()
			
		else
			mSleep(500)
		end
		
	end
end


function checkAndRepeatActBattle(...)
	tap(598, 164)
	mSleep(1000)
	adaptBattleMapPage()
	
	if isChooseBattlePage() then
		mlog(" is choose battle page")
		
		if config.actBattle.battle == true and isActivityBattle() then
			activityBattleAndRepeat()
			config.battle = config.material
		end
		
	end
end


function is1_1( ... )
	if (isColor( 246, 1096, 0xc6c07e, 85) and 
isColor( 244, 1092, 0x6c6634, 85) and 
isColor( 233, 1092, 0x4d4723, 85) and 
isColor( 264, 1098, 0x7b7038, 85)) then
return true
end 
if (isColor( 296,  981, 0x9cca3b, 85) and 
isColor( 360, 1007, 0x85b629, 85) and 
isColor( 365, 1044, 0x86b22b, 85)) then
 return true
end
return false

end

function fpBattle()
	config.battle = config.fpBattle
	mLog("fp battle begin")	
	
	index = 0
	while(true) do
		
		adaptAllOtherPages()
		--adaptBattleMapPage()
		
		-- if index % 11 == 0 then
		-- 	checkAndRepeatActBattle()
		-- 	index = index + 1
		-- 	mSleep(1000)
		-- 	tap(598, 164)
			
		-- end
		
		--点击地图1-1
		if(is1_1()==true)  then
			
			tap(258, 1091)
			mSleep(3500)

			mLog("is battle page: 1-1-5")
			mlog("进入fp副本")
			
			chooseFpBattle()
			mSleep(800)
			waitForChooseHeroPage()
			chooseHero(1)

		end

		if (isColor( 514, 1147, 0xdbd9d3, 85) and 
isColor( 569, 1147, 0x837a55, 85) and 
isColor( 573, 1150, 0xfbfafa, 85) and 
isColor( 580, 1156, 0x847d5f, 85) and 
isColor( 553, 1173, 0xd2cfc1, 85)) then
			chooseFpBattle()
			mSleep(800)
			waitForChooseHeroPage()
			chooseHero(1)
		end
		
		index = index + 1

		-- if(isFpBattlePage()) then
		-- 	index = index + 1
		-- 	mLog("is battle page: 1-1-5")
		-- 	mlog("进入fp副本")
			
		-- 	chooseFpBattle()
		-- 	mSleep(800)
		-- 	waitForChooseHeroPage()
		-- 	chooseHero(1)
			
		-- end
		
		
		
		mSleep(500)
		
	end
end

function tryChangeBattleConfig(...)
	if config.actBattle.battle == true then
		if(isActivityBattle()) then
			if config.exeBattle.battle == true and isExeActivity() then
				mlog("change config.battle to config.exeBattle")
				config.battle = config.exeBattle
			elseif config.goldBattle.battle == true and isGoldActivity() then
				config.battle = config.goldBattle
				mlog("change config.battle to config.goldBattle")
			elseif config.goldBattle.battle == true and isScollActivity() then
				config.battle = config.scrollBattle
				mlog("change config.battle to config.scrollBattle")	
			end
		end
	end
end

local function onceBattle(...)
	mlog("进入副本:" .. config.battle.name)
	chooseBattle(config.battle.level)
	waitForChooseHeroPage()
	chooseHero(config.battle.heroNumber)
	if isFullBagDialog() then
		adaptFullBagDislog()
	end
end


function materialBattleAndRepeat(...)
	
	config.battle = config.material
	
	while true do
		adaptAllOtherPages()
		adaptBattleMapPage()
		
		if isChooseBattlePage() then
			mlog(" is choose battle page")
			
			if config.actBattle.battle == true and isActivityBattle() then
				activityBattleAndRepeat()
				config.battle = config.material
			end
			
			onceBattle()
		end
		
	end
end

function onlyActivityBattle(...)
	while true do
		-- body
		tap(981, 1587)
		if isActivityBattle() then
					activityBattleAndRepeat()

		end
		mSleep(1000)
		mSleep(10 * 1000)
		adaptBattleMapPage()
		mSleep(1000)
		if isActivityBattle() then
					activityBattleAndRepeat()

		end
	end
end

function activityBattleAndRepeat(...)
	mlog("activityBattleAndRepeat")
	tryChangeBattleConfig()
	
	while true do
		
		if config.buyEnergy.leftTimes == 0 and config.buyEnergy.allow == true
		and config.battle.buyEnergy == true then
			config.buyEnergy.leftTimes = 1
		end
		
		if isChooseBattlePage() then
			mlog(" is choose battle page")
			if not isActivityBattle() then
				break
			end
			
			onceBattle()
		end
		
		adaptAllOtherPages()
		adaptBattleMapPage()
		
	end	
end


