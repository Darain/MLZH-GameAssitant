require("util")

require("dialogPage2")

-- 普攻时的像素位移
local delta_pix = 160
-- 普攻的延迟，太小不好
local delay_time = 70
-- 普攻的坐标
local const_y = 790
local begin_x = 100
local end_x = 1020

function isBattleEnd()
	if (isColor(488, 1404, 14075299, 85) and isColor(524, 1407, 131586, 85) and isColor(584, 1407, 14075299, 85) and isColor(457, 1420, 14075299, 85) and isColor(510, 1419, 65793, 85) and isColor(548, 1424, 0, 85) and isColor(595, 1424, 14075299, 85) and isColor(616, 1422, 14272678, 85)) then
		return true
	else
		return false
	end
end

local function isHeroRound(...)
	m = 75
	if(isColor(107, 1335, 8549960, 85) and
	isColor(111, 1335, 15915362, 85) and
	isColor(116, 1342, 14794828, 85) and
	isColor(114, 1327, 16116928, 85) and
	isColor(114, 1322, 15527133, 85) and
	isColor(91, 1322, 5074592, 85) and
	isColor(100, 1357, 3361901, 85) and
	isColor(129, 1325, 4405805, 85) and
	isColor(147, 1300, 5462875, 85) and
	isColor(94, 1384, 5133910, 85)) then
		return true
	else
		return false
	end
end

function isBattleBegin()
if (isColor(  72, 1885, 0x3c80df, 85) and 
isColor(  72, 1887, 0x2c60c2, 85) and 
isColor(  72, 1902, 0x28313f, 85) and 
isColor(  76, 1901, 0x355fa8, 85) and 
isColor(  73, 1885, 0x3c80df, 85) and 
isColor(  76, 1887, 0x2c60c9, 85)) then
		return true
	else
		return false
	end
end

function battleEnd()
	while(true) do
		if(isBattleEnd()) then
			screenShot("battle_result")
			mSleep(200)
			
			tap(457, 1400)
			
			mSleep(800)
			
			break
		else
			mSleep(1000)
		end
		mSleep(1000)
	end
end

local function clickMonsters(...)
	for var = begin_x, end_x, delta_pix do
		tap(var, const_y)
		mSleep(delay_time)
	end
end

local function clickSkill(index)
	-- body
	local y = 1340
	local x = 265
	local p = 170
	
	tap(x + index * p, y)
	
end

local function tryUseAttackSkills(...)
	local i = math.random(3)
	clickSkill(i)
	mSleep(100)
	clickMonsters()
	
	if isHeroRound() then
		clickSkill(0)
		clickMonsters()
	end
	
end


local function tryUseSkills(...)
	
end


-- TODO 
local function heroDoAction(...)
	
	if config.fight.useAttackSkill then
		--mlog(tostring(useAttackSkill))
		tryUseAttackSkills()
		return
	end
	
	clickMonsters()
	
end

function isBattleDefeat(...)
	-- body
	if(isColor(374, 1432, 11544629, 85) and
	isColor(380, 1420, 8532012, 85) and
	isColor(377, 1411, 14272678, 85) and
	isColor(391, 1428, 15888015, 85) and
	isColor(393, 1439, 16777215, 85) and
	isColor(407, 1436, 15486807, 85) and
	isColor(703, 1431, 5985093, 85) and
	isColor(706, 1430, 14075299, 85) and
	isColor(717, 1425, 263171, 85)) then
		return true
	else
		return false
	end
end

function battleDefeat(...)
	-- body
	mlog("战斗失败")
	screenShot("defeat battle")
	tap(719, 1440)
	mSleep(500)
	tap(360, 900)
	mSleep(500)
end



function waitForHeroRound(...)
	-- body
	while true do
		-- bodd
		if(isColor(91, 1372, 5460819, 85) and
		isColor(89, 1314, 5725531, 85) and
		isColor(107, 1317, 10130042, 85) and
		isColor(111, 1335, 15915362, 85) and
		isColor(115, 1345, 13939536, 85) and
		isColor(114, 1324, 15132375, 85) and
		isColor(120, 1342, 5984053, 85) and
		isColor(118, 1335, 16112748, 85) and
		isColor(955, 1359, 12562538, 85) and
		isColor(954, 1341, 16514015, 85)) then
			return
		end
		mSleep(200)
	end
end

function beginBattle()
	mlog("开始战斗")
	while(true) do

		:: m1 ::

		adaptServerErrorPage2()

		if isBattleEnd() then
			battleEnd()
			break
		end
		
		if isBattleDefeat() then
			-- body
			battleDefeat()
			break
		end
		
		if config.fight.quickAttack==true then
			-- body
			clickMonsters()
			goto m1
		end
		
		if isHeroRound() then
			heroDoAction()
		end

		
		
		mSleep(200)
	end
end 