require("util")

local function closeBuyEnergyPage(...)
	--先点2个叉
	tap(966, 371)
	mSleep(800)
	tap(958, 230)
	mSleep(800)
	
	--然后返回选择副本界面
	tap(417, 621)
	
end


local function buyEnergy()
	
	--点击购买
	mlog("buyEnergy()")
	tap(825, 533)
	mSleep(400)
	
	tap(369, 1061)
	
	mSleep(2000)
	
	while(true) do
		if(isColor(963, 587, 5204120, 85) and
		isColor(966, 605, 8219715, 85) and
		isColor(963, 611, 7954487, 85) and
		isColor(963, 622, 5658454, 85) and
		isColor(963, 625, 5400715, 85) and
		isColor(963, 629, 4350868, 85) and
		isColor(941, 624, 16711421, 85)) then
			
			tap(966, 605)
			mSleep(1500)
			
			
			break
		end
		mSleep(500)
		
	end

	mSleep(1500)

	mlog("购买体力完成")

end 

function isBuyEnergyPage(...)
    if (isColor(129, 528, 0x682f89, 85) and isColor(160, 528, 0x292624, 85) and isColor(200, 530, 0xaa975f, 85) and isColor(198, 508, 0x333333, 85) and isColor(815, 526, 0xe74e5f, 85) and isColor(856, 542, 0xfdfdfd, 85) and isColor(850, 551, 0xf8f9f9, 85) and isColor(862, 551, 0xdedfdf, 85) and isColor(874, 552, 0xf5f6f6, 85)) then
        return true
    else
        return false
    end
end

function adaptBuyEnergyPage(...)
	--购买体力页面
	if isBuyEnergyPage() then
		mlog("购买体力界面 level:"..config.battle.level)
		mlog("剩余购买次数:"..config.buyEnergy.leftTimes)
		--减小副本等级
		if(level == 2) then
			if(config.buyEnergy.allow and config.buyEnergy.leftTimes > 0) then
				
				buyEnergy()

				config.battle.level = config.battle.startLevel
				config.buyEnergy.leftTimes = config.buyEnergy.leftTimes - 1
				mlog("购买体力成功，剩余购买次数:" .. config.buyEnergy.leftTimes)

			end
		else
			level = 2
		end
		
		closeBuyEnergyPage()
		
		mSleep(800)
		
	end
end