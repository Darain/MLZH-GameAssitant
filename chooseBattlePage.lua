require("util")

require("chooseHeroPage")

function isStoneBattle(...)
	if(isColor(368, 910, 16777215, 85) and isColor(369, 896, 15987955, 85) and isColor(365, 900, 2766117, 85) and isColor(373, 900, 2305566, 85) and isColor(438, 902, 15329768, 85) and isColor(438, 905, 2962218, 85) and isColor(455, 905, 16119284, 85) and isColor(458, 905, 792581, 85) and isColor(488, 902, 15987954, 85) and isColor(488, 900, 5199946, 85)) then
		return true
	else
		return false
	end
end


function isChooseBattlePage()
	m = 85
	if(isColor(373, 739, 14927961, m) and isColor(369, 764, 15585626, m) and isColor(356, 752, 3681884, m) and isColor(380, 760, 3485016, m) and isColor(388, 756, 15782747, m) and isColor(392, 747, 4076902, m) and isColor(387, 739, 15059289, m) and isColor(380, 738, 4275059, m)) then
		return true
	else
		return false
	end
end

local function clickBattle(index)
	if not index then
		index = 1
	end
	tap(500, 900 +(index - 1) * 150)
	mSleep(1000)
end

local function clickSecondBattle(...)
	mSleep(1000)
	tap(495, 1051)
	mSleep(1000)
end

local function clickMatirialBattle(...)
	clickBattle(2)
end

local function selectMaxLevelBattle()
	for var = 1, 100 do
		if(isColor(402, 1249, 16711421, 85) and isColor(397, 1232, 16382456, 85) and isColor(396, 1243, 16513786, 85)) then
			tap(470, 1223)
			break
		else
			mSleep(200)
		end
	end
end

local function selectMiddleLevelBattle(...)
	for var = 1, 100 do
		if(isColor(319, 934, 16184818, 85) and isColor(329, 928, 15658213, 85) and isColor(329, 955, 16382198, 85) and isColor(340, 940, 15592681, 85) and isColor(335, 940, 5721653, 85) and isColor(324, 940, 5457455, 85)) then
			tap(532, 934)
			mSleep(500)
			break
		end
		mSleep(200)
	end
end

local function selectLegandLevelBattle(...)
	tap(524, 1310)
	mSleep(400)
end


local function clickBattleLevel(level)
	-- body
	if level == 4 then
		selectMaxLevelBattle()
	end
	if level == 5 then
		selectLegandLevelBattle()
	end
	
	if level == 2 then
		selectMiddleLevelBattle()
	end
end

function isBattleMapPage(...)
	if(isColor(917, 1630, 13154440, 85) and
	isColor(938, 1630, 13284743, 85) and
	isColor(1000, 1576, 7484290, 85) and
	isColor(985, 1565, 8535442, 85) and
	isColor(970, 1565, 5322890, 85) and
	isColor(961, 1588, 2695205, 85) and
	isColor(1025, 1632, 13284486, 85)) then
		return true
	else return false
	end
end

function chooseBattle(...)
	
	if isChooseBattlePage() then
		
		if config.battle.isActBattle == true then
			clickSecondBattle()
			mSleep(700)
			mlog("click level:" .. config.battle.level)
			clickBattleLevel(config.battle.level)
			return
		end
		
		if config.battle.isMaterial == true then
			clickBattle()
			mSleep(700)
			mlog("click level:" .. config.battle.level)
			clickBattleLevel(config.battle.level)
			return
		end
		
	end
end

function adaptBattleMapPage( ... )
    if isBattleMapPage() then
        if isChooseBattlePage() then
            return 
        else
            tap(917,1630)
        end 
    end
end

