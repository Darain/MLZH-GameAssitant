
require("util")

local function removeRepeatHero(...)
	if(isColor(933, 484, 14306869, 85) and isColor(922, 495, 14241334, 85) and isColor(920, 471, 13782583, 85) and isColor(944, 496, 13782583, 85) and isColor(944, 472, 13847867, 85)) then
		tap(930, 378)
		mSleep(500)
		
		-- 加入自己的第四个英雄
		tap(348, 633)
		mSleep(400)
		-- 点击我的角色
		tap(260, 1376)
		mSleep(500)
	end
	
end

local function isQuenePage(...)
	if(isColor(510, 206, 13224396, 85) and isColor(504, 229, 15658734, 85) and isColor(502, 237, 7763580, 85) and isColor(516, 234, 9868955, 85) and isColor(576, 240, 8487302, 85) and isColor(573, 237, 16777215, 85) and isColor(566, 206, 14671840, 85) and isColor(566, 205, 9408404, 85) and isColor(554, 239, 11776950, 85)) then
		return true
	else
		return false
	end
end

local function removeAllHeros(...)
	
	-- body
	if(not isQuenePage()) then
		return
	end
	
	mSleep(300)
	
	local x = 282
	
	local y = 380
	
	tap(x, y)
	mSleep(400)
	
	tap(x + 200, y)
	mSleep(400)
	
	tap(x + 400, y)
	mSleep(400)
end

local function addTopHeros(index)
	local x = 227
	local y = 763
	
	for i = 1, index do
		tap(x, y + i * 236 - 236)
		mSleep(300)
	end
	
end



local function couldClickBeginBattle()
	if(isColor(437, 1541, 7037522, 85) and isColor(435, 1556, 7037521, 85) and isColor(639, 1542, 7037778, 85) and isColor(636, 1565, 6906192, 85) and isColor(676, 1543, 6774602, 85) and isColor(663, 1569, 6840142, 85)) then
		return false
	else
		return true
	end
end

local function clickBeginBattle()
	tap(551, 1558)
	mSleep(500)
end



local function isChooseHeroPage(...)
	
	if(isColor(266, 389, 16777215, 85) and
	isColor(261, 387, 5592407, 85) and
	isColor(291, 387, 16777215, 85) and
	isColor(295, 387, 5265496, 85) and
	isColor(478, 388, 16777215, 85) and
	isColor(472, 380, 5789793, 85) and
	isColor(482, 395, 5726047, 85) and
	isColor(493, 386, 16777215, 85) and
	isColor(670, 393, 5987163, 85) and
	isColor(694, 385, 16777215, 85)) then
		return true
	else
		return false
	end
end



function waitForChooseHeroPage(...)
	mlog("等待 选择英雄界面")
	local time = 1000 * 5
	for i = 1, 50 do
		-- body
		if isChooseHeroPage() then
			break
		end
		mSleep(100)
	end
end

function adaptIfSortByEnergy(...)
	
	if not(isColor(300, 1421, 13948116, 85) and
	isColor(300, 1419, 3750201, 85) and
	isColor(300, 1423, 15987699, 85) and
	isColor(290, 1428, 15790320, 85) and
	isColor(287, 1428, 5066061, 85) and
	isColor(283, 1428, 14671839, 85) and
	isColor(257, 1428, 12829635, 85) and
	isColor(255, 1428, 4934475, 85) and
	isColor(259, 1449, 15263976, 85) and
	isColor(259, 1451, 4605510, 85)) then
		mlog("调整为按体力排序")
		tap(300, 1421)
		mSleep(200)
		tap(250, 1347)
	end
end


local function addFriendHero(...)
	
	--如果不是购买体力框

	tap(753, 626)
	mSleep(800)
	
	-- 切换为按职业逆序排列
	if not (isColor( 225, 1425, 0xfefefe, 85) and 
isColor( 251, 1425, 0x888888, 85) and 
isColor( 292, 1424, 0xa2a2a2, 85) and 
isColor( 347, 1430, 0x6e6e6e, 85) and 
isColor( 378, 1433, 0x444444, 85) and 
isColor( 381, 1433, 0x737373, 85)) then
		tap(265, 1432)
		mSleep(400)
		tap(295, 1355)
		mSleep(400)
	end
	
	--若第3个英雄头像是法师
-- 	if (isColor( 139, 1155, 0xfcaafd, 85) and 
-- isColor( 148, 1164, 0xfefdfe, 85) and 
-- isColor( 159, 1164, 0x9b3fc5, 85) and 
-- isColor( 159, 1175, 0x6c565e, 85) and 
-- isColor( 159, 1177, 0x4e3155, 85)) then
-- 		return ...
-- 		--tap()
-- 	end

	mSleep(200)
	tap(218, 1221)
	mSleep(400)

end



function chooseHero(num)
	
	if isChooseHeroPage() then
		
		adaptIfSortByEnergy()
		
		mlog("进入战斗前选择英雄界面")
		mlog("选择英雄数量:" .. num)
		
		removeAllHeros()
		addTopHeros(num)



		mSleep(500)
		
		if isBuyEnergyPage() then
			return 
		end

		if config.battle.useFriendHero==true then
			mlog("使用好友英雄")
			addFriendHero()

		end

		removeRepeatHero()
		
		mSleep(400)

		if couldClickBeginBattle() then
			mlog("click begin battle button")
			clickBeginBattle()
		end
		
		mSleep(300)
		
		--人数不满确认框
		if(isColor(316, 1052, 14075299, 85) and
		isColor(385, 1057, 14075299, 85) and
		isColor(692, 1065, 14075299, 85) and
		isColor(760, 1066, 14075299, 85) and
		isColor(372, 1063, 65793, 85) and
		isColor(356, 1058, 0, 85) and
		isColor(336, 1067, 0, 85) and
		isColor(331, 1077, 855050, 85) and
		isColor(700, 1058, 0, 85) and
		isColor(721, 1068, 0, 85)) then
			tap(316, 1052)
		end

		--副本时间已到确认框

		
		mlog("选择英雄结束")
		
		mSleep(800)
	end
end

