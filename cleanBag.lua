require("util")


local function isGreenEquip(x, y)
	if(isColor(861, 628, 2633512, 85) and
	isColor(867, 520, 2898993, 85) and
	isColor(1008, 579, 3098935, 85) and
	isColor(960, 498, 2833708, 85)) then
		return true
	else return false
	end
end

local function clickLeftBtn(...)
	-- body
	tap(183, 383)
	mSleep(300)
end

local function clickRightBtn(...)
	tap(449, 376)
	mSleep(300)
	-- body
end



local function clearEquip(...)
	-- body
	clickSellBtn()
end

function isFullBagDialog(...)
	-- body
	if(isColor(301, 1026, 131329, 85) and
	isColor(293, 1023, 14075299, 85) and
	isColor(603, 1041, 16040404, 85) and
	isColor(619, 1044, 15293291, 85) and
	isColor(612, 1055, 8855337, 85) and
	isColor(612, 1061, 4208687, 85) and
	isColor(634, 1036, 9971761, 85) and
	isColor(635, 1024, 8414035, 85)) then
		return true
	else return false
	end
end

local function clickScrollBag(...)
	tap(650, 250)
end



local function isScrollBagSort(...)
	if(isColor(95, 1609, 8750469, 85) and
	isColor(108, 1610, 12961221, 85) and
	isColor(108, 1608, 5395026, 85) and
	isColor(337, 1613, 13750737, 85) and
	isColor(336, 1610, 4539717, 85) and
	isColor(239, 1635, 10197915, 85) and
	isColor(237, 1635, 5066061, 85)) then
		return true
	else return false
	end
end

local function waitForBagPage(...)
	while true do
		if(isColor(514, 18, 14473954, 85) and
		isColor(514, 16, 3879772, 85) and
		isColor(510, 22, 4011606, 85) and
		isColor(514, 22, 6051441, 85) and
		isColor(517, 22, 13289683, 85) and
		isColor(520, 22, 12697548, 85) and
		isColor(522, 22, 4735335, 85) and
		isColor(531, 22, 4866921, 85) and
		isColor(527, 22, 15790067, 85)) then
			break
		end
		mSleep(300)
	end
end

function shouldCleanScrollFirstRow(...)
	if(isColor(85, 501, 6961441, 85) and
	isColor(215, 499, 7161127, 85) and
	isColor(87, 629, 6699300, 85) and
	isColor(213, 635, 6438943, 85)) then
		return true
	end
	if(isColor(875, 504, 9015695, 85) and
	isColor(869, 633, 11584946, 85) and
	isColor(996, 627, 6383463, 85) and
	isColor(990, 499, 6056528, 85)) then
		return true
	end
	return false
	
end

function clickFirstRow(...)
	local x = 154
	local y = 567
	
	for i = 0, 4 do
		tap(x + i * 200, y)
		mSleep(300)
	end
end

function shouldCleanEquipFirstRow(...)
	if(isColor(861, 628, 2633512, 85) and
	isColor(867, 520, 2898993, 85) and
	isColor(1008, 579, 3098935, 85) and
	isColor(960, 498, 2833708, 85)) then
		return true
	else 
		return false
	end
end

function cleanEquipBag(...)
	tap(420, 240)
	mSleep(800)
	clickRightBtn()
	
	mSleep(500)
	
	--按天梯等级由低到高排序
	tap(237,1620)
	mSleep(500)
	tap(233,1080)
	mSleep(500)

	while true do
		if shouldCleanEquipFirstRow() == true then
			clickFirstRow()
			clickRightBtn()
			mSleep(400)
			tap(353, 1242)
			mSleep(1500)
			--关闭金币
			tap(555, 1306)
			mSleep(2000)
				clickRightBtn()
				mSleep(500)
		else
			return ...
		end
	end
	
	
	
end

function cleanScrollBag(...)
	clickScrollBag()
	mSleep(800)
	if not isScrollBagSort() then
		tap(273, 1617)
		mSleep(300)
		tap(285, 1407)
		mSleep(500)
	end
	while true do
		if shouldCleanScrollFirstRow() == true then
			clickLeftBtn()
			clickFirstRow()
			clickRightBtn()
			mSleep(400)
			tap(353, 1242)
			mSleep(1500)
			--关闭金币
			tap(540, 1043)
			mSleep(2000)
		else
			break
		end
		
	end
	
	
end


function adaptFullBagDislog(...)
	--点击直通背包
	tap(370, 1040)
	
	waitForBagPage()
	
	
	cleanEquipBag()
	mSleep(1000)
	
	cleanScrollBag()
	mSleep(2000)
	
	tap(534, 1792)
end 