require("TSLib")

nLog("加载config")

config = {battle = {}, fight = {}}

-- 体力设定
config.buyEnergy = {allow = true, leftTimes = 0}


-- 加好友设定
config.addFriendAfterBattle = false

config.fpBattle = {useFriendHero=true,fight = config.fight.quickFight,isMaterial = false,name = "fp", heroNumber = 1}


--
config.material = {useFriendHero=true,fight = config.fight.quickFight, isMaterial = true, name = "material", battle = true, startLevel = 4, level = 4, nextLevel = 2,
heroNumber = 1}

--活动副本设定	
config.actBattle = {useFriendHero=true,buyEnergy=true,isActBattle = true, name = "act", battle = true, startLevel = 5,             level = 5, nextLevel = 2, heroNumber = 3}

config.goldBattle = {useFriendHero=true,buyEnergy=false,isActBattle = true, name = "gold", battle = true, startLevel = 5,           level = 5, nextLevel = 5, heroNumber = 3}
config.scrollBattle = {useFriendHero=true,buyEnergy=true,isActBattle = true, name = "scroll", battle = true, startLevel = 4,       level = 4, nextLevel = 4, heroNumber = 3}
config.exeBattle = {useFriendHero=true,buyEnergy=false,isActBattle = true, name = "exe", battle = true, startLevel = 5,             level = 5, nextLevel = 5, heroNumber = 3}

-- 战斗设定
config.fight.quickFight = {quickAttack = true, useAttackSkill = false}

-- 背包设定
bag = {}
bag.equip = {}
bag.equip.green = {sell = false, dispose = true}
bag.equip.blue = {sell = false, dispose = false}

bag.scroll = {}
bag.scroll.q1 = {sell = true}
bag.scroll.q2 = {sell = true}

