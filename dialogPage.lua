require("util")
require("buyEnergyPage")
require("battle")
require("cleanBag")

local function adaptAchievementDialog(...)
	-- body
	if(isColor(567, 639, 15000805, 85) and
	isColor(510, 1209, 789000, 85) and
	isColor(506, 1214, 8484450, 85) and
	isColor(505, 1216, 10260342, 85) and
	isColor(560, 1220, 131329, 85) and
	isColor(560, 1215, 12759956, 85) and
	isColor(563, 1208, 13154456, 85) and
	isColor(564, 1243, 14075299, 85) and
	isColor(524, 1227, 12036491, 85) and
	isColor(521, 1227, 1315343, 85)) then
		tap(524, 1225)
		mSleep(400)
	end
end

function adaptServerErrorPage( ... )
	if (isColor( 366, 1059, 0x151310, 85) and 
isColor( 366, 1057, 0xa79a80, 85) and 
isColor( 957,  587, 0x45597d, 85) and 
isColor( 938,  580, 0xffffff, 85) and 
isColor( 929,  580, 0x3f537c, 85)) then
	tap(366,1059)
	end
end

local function adaptGrowHeloPage(...)
	if(isColor(942, 372, 3486518, 85) and
	isColor(941, 382, 11580084, 85) and
	isColor(951, 394, 14474718, 85) and
	isColor(915, 406, 2960173, 85) and
	isColor(599, 376, 16777215, 85) and
	isColor(586, 389, 16777215, 85) and
	isColor(532, 393, 3481141, 85) and
	isColor(918, 364, 4144959, 85) and
	isColor(966, 359, 13290187, 85) and
	isColor(966, 353, 3025454, 85)) then
		
		tap(940, 385)
	end
	
end

local  function addFriend()
	tap(536, 1129)
	:: mm ::
	
	if(isColor(956, 590, 10721410, 85) and
	isColor(932, 610, 3495552, 85) and
	isColor(949, 589, 14998458, 85) and
	isColor(952, 601, 7888950, 85) and
	isColor(952, 615, 16316135, 85) and
	isColor(988, 616, 4349579, 85) and
	isColor(978, 618, 14932666, 85) and
	isColor(978, 575, 3493495, 85) and
	isColor(962, 580, 4021901, 85) and
	isColor(963, 608, 11180400, 85)) then
		tap(956, 590)
	else
		mSleep(300)
		--goto mm
	end
	mSleep(400)
	
	if(isColor(960, 650, 5071754, 85) and
	isColor(956, 660, 14996410, 85) and
	isColor(953, 679, 16579305, 85) and
	isColor(948, 669, 5594994, 85) and
	isColor(986, 669, 4943005, 85) and
	isColor(980, 653, 16381935, 85) and
	isColor(971, 682, 7692601, 85) and
	isColor(965, 687, 6251886, 85) and
	isColor(984, 693, 8153657, 85)) then
		tap(960, 650)
	end
end


local function adaptBattleBegin(...)
	if isBattleBegin() then
		beginBattle()
		mSleep(500)		
	end
	mSleep(400)
end


local function adaptAddFriendDialog()
	if(isColor(938, 654, 5723468, 85) and isColor(953, 655, 14735542, 85) and isColor(976, 651, 15330025, 85) and isColor(978, 671, 5264212, 85) and isColor(941, 671, 4416661, 85) and isColor(955, 681, 11707782, 85) and isColor(989, 667, 4219282, 85) and isColor(960, 693, 5532826, 85) and isColor(962, 674, 13680025, 85) and isColor(963, 683, 7231800, 85)) then
		if config.addFriendAfterBattle == true then
			-- 添加好友
			mlog("添加好友")
			mSleep(400)
			addFriend()
		else
			--关闭窗口
			tap(963, 683)
		end
	end
	
	mSleep(800)
end

function adaptAllDialogPage(...)
	adaptServerErrorPage()
	adaptAchievementDialog()
	adaptGrowHeloPage()
	adaptAddFriendDialog()
	adaptBattleBegin()
	adaptBuyEnergyPage()
	if (isFullBagDialog()) then 
	adaptFullBagDislog()
	end 
end 