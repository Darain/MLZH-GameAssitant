require("TSLib")
require("config")

function time()
    -- body
    current_time = os.date("%Y-%m-%d %H:%M:%S", os.time())
    return current_time
end

function mLog(info)
    -- body
    s = time() .. " " .. info
    log(s)
    nLog(s)
end

function mlog(info)
	return mLog(info)
end

function screenShot(name)
    if not name then
        name = ""
    end
    
    current_time = os.date("%Y-%m-%d %H:%M:%S", os.time()) --以时间戳命名进行截图
    w, h = getScreenSize()
    -- 右下角顶点坐标最大为 (宽度最大值-1, 高度最大值-1)
    
    fileName = name .. " " .. current_time .. ".png"
    path = userPath() .. "/res/battle_result/"
    mlog(fileName)
    --snapshot("battle_result/"..fileName, 0, 0, w-1, h-1)
    snapshot(fileName, 0, 0, w - 1, h - 1)
end
